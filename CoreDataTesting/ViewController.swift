//
//  ViewController.swift
//  CoreDataTesting
//
//  Created by BTB_015 on 12/10/20.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    // Step 2 Crete context for save to CoreData
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    // Step 3 constant of Note for CoreData
    var note: [Note] = []
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        fetchData()
//        postData()
//        remove()
        update()
        
        
//        print(note![0].title!)
//        print(note)
        
        for i in note {
            print(i.title!)
        }
    }
    //Get Dat
    func fetchData() {
        self.note = try! context.fetch(Note.fetchRequest())
    }
    
    //put Data to Table
    func postData()  {
        //Create for accessing atribute at table CoreData
        let contact = Note(context: self.context)
        contact.title = "Long"
        try! context.save()
    }
    
    //remove Data from table
    func remove() {
        let itemForDelete = self.note.last
        self.context.delete(itemForDelete!)
        try! context.save()
    }
    
    //upadta data
    func update() {
        let itemforUpdate = note.first!
        
        itemforUpdate.title  = "New Update"
        try! context.save()
    }
}

